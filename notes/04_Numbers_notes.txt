1,18.	O miso ma ba-Israel esengeli ’te moto nyonso ayeba bankoko ba ye, akoka kotanga bango, soki te amononi moto wa solo wa ekolo ya Nzambe te.
1,32.	Libota lya Yozefu lisalaki linene mpenza, yango wana bakabolaki bango na mabota mabale, ba-Efraim na ba-­Manase.
1,46.	Motango moye mwa 603 550 mo­­leki mwa moke moye motangemi o Bob 12,37, kasi moleki mingi motango mwa bango mwa solo. Tobosana te ’te bazalaki kotanga bato te lokola bakosalaka yango sikawa. Bakomi balingaki bobele koyebisa ’te ba-Israel bazalaki ebele mpenza.
3,3.	Bobele Aron na bana na bankoko ba ye baponomoki mpo bazala banganga Nzambe. Ba-Levi basusu banso baponomi mpo basala misala misusu mya Nza-mbe, basalisa bongo banganga Nzambe.
3,4.	Tala Lv 10,1-3.
5,16.	Mpo bato bango moko bakokaki koyeba te soko mwasi oyo afundami na ntina to te, basengeki Nzambe akata likambo lya ye.
6,1.	Na nkombo ‘nazir’ bazalaki kolakisa moto oyo abonzami na Nzambe, to mwa eleko to eleko elai. Samson na Samuel (Baz 13,3-7 na 1 Sam 1,11) bamibonzaki na Nzambe bomoi bwa bango mobimba, kasi bato mingi baumisi eleko ena mingi boye te.
6,23.	Eye ezali ndakisa ya maloba ma bobenisi, kasi o buku ya Nzembo ya Davidi ndakisa isusu izali mpe mingi.
7,89.	Tala Bob 25,17+.
9,15.	Limpata lilakisi ’te Nzambe azalaki o kati ya bato ba ye mpo akamba mpe abatela bango (Bob 13,21).
10,33.	Sanduku ya Bondeko mpe ezalaki kolakisa ’te Nzambe azalaki kokamba bato ba ye bipai binso.
11,1.	Móto mwa Yawe : minkalali mibeteli biema o nganda, mpe mitumbi bya­ngo.
11,4.	Ntango ba-Israel babimaki o Ezipeti, bato basusu bakendeki na bango (Bob 12,38) ; bango nde bato babandi kolobaloba.
11,7.	‘Man-hu’ : tala Bob 16,15+.
11,25.	Mosakoli to profeta : moto oyo akolobaka o nkombo ya Nzambe.
11,28.	Yozue, mwana wa Nun, azalaki mosaleli wa Moze (Bob 17,9 ; 24,13 ; 33,11), na nsima akitani ye (Yoz 1).
12,1.	Mwasi wa Kusi : mbele ezalaki Sipora, mwana wa Yetro, nganga Nzambe wa Madian (Bob 2,21).
13,1.	Kanana ezalaki pene na Mbu Enene ya Westi, o Nordi ya eliki ya Paran.
13,16.	Nkombo Yozue lokola ’te ‘Nza­mbe akobikisaka’. Moze alakisi bongo ’te Nzambe akobikisa bato ba ye na lisalisi lya elenge mobali oyo.
13,28.	Ba-Anak bazalaki bato balai mpe bilombe o bitumba.
16,1.	Kore, Datan na Abiram bazalaki ba-Levi ; Kore na bato ba ye bayokeli Moze na Arone zuwa, mpo bolingaki ’te bango mpe bakoka kobonzela Yawe mabonza. Datan na Abiram bandimi boya­ngeli bwa Moze te.
17,16.	Lisolo liye lilakisi ’te Nzambe aponi Aron mpo azala moyangeli wa milulu mya Nzambe ; bana ba ye bakokitana ye o mosala moye.
20,12.	Toyebi te ndenge nini Moze azangi boyambi ? Mpo abeti libanga na lingenda mbala ibale ? To abangi kopalela bato bazalaki kolobaloba (20,2-5) ?
20,21.	Ba-Kanana bapekisaki ba-Israel kokoto o mokili mwa bango pene na Kades (14,39-45). Ekomami polele te nzela nini ba-Israel balandaki na nsima. Moze asengi mokonzi wa Edom nzela ya kokatisa ekolo ya ye, kasi ye aboyi, bongo Moze alandi nzela elai, mpe akatisi eliki lisusu. Ba-Israel bakomi bongo epai ya Esti ya Mbu ya Liwa liboso lya kokatisa ekolo ya Moab, mpo ya kokoma o eteni ya Esti ya ekolo ya ba-Kanana, esika ebale Yordane ekotiolaka o Mbu ya Liwa.
21,9.	Moto atali ekeko ya nyoka bobele na miso akobika te, asengeli mpe kosenge Nzambe bolimbisi mpe kobondela ye. Ekeko eye etiamaki na nsima o Te­mpelo ya Yeruzalem, kasi mokonzi Ezekia alongoli yango (2 Bak 18,4), mpo bato mingi babandaki kokumbamela yango.
21,21.	Sikon azalaki monguna monene wa ba-Israel, kasi na lisalisi lya Nzambe bakokaki kolonga ye.
22,1.	Profeta Balame, oyo afandaki penepene na Efrate, ayebanaki mingi. Ata azalaki kokumisa banzambe basusu, azalaki kondima ’te Yawe aleki bango.
22,22.	Ntina nini bokei bwa Balame boyokisi Yawe nkanda ? O 20,20 totangi te ’te Nzambe atindi ye bongo ? Mbele moto akomi buku eye ayokaki masolo mabale. Awa akuti likambo lilobami na ndenge ibale, ayebi te nini asengeli kopono ; bongo mbala esusu akomi eye ayoki o lisolo lya liboso, mbala esusu akomi eye ayoki o lisolo lya mabale.
22,33.	Mpunda emoni eye profeta mopagano akoki komono te ! Tala likambo lya kokamwa. Lisolo liye lisali lokola lisapo, mpe mokomi alingi se koteya ’te : ata nyama eleki moto na mayele soko aboyi kotosa Nzambe.
24,7.	Ba-Israel baponi mokonzi wa ekolo bobele mibu 200 na nsima. Bako­nzi ba liboso, Saul na Davidi, bakolonga ba-Amalek (1 Sam 15,1-9). Mbele babakisi maloba maye (24,7) na nsima.
24,17.	O mikili mingi bazalaki kolakisa mokonzi wa ekolo lokola monzoto mokongengeke. Awa balakisi na bobo­mbami bokonzi bwa nkembo bwa Davidi, mpe na nsima boyei bwa Mesiya (Lk 1,78).
25,6.	Mokomi asangisi nkombo ibale o lisolo liye, Moab na Madian, mpe ya solo ba-Moab na ba-Madian basanganaki mingi o ekolo eye. Na lisolo liye lya ba-Moab na ba-Madian babendaki bato banso o nzela ya masumu, mokomi ali­ngi kolimbola bonguna boye bozalaki ntango inso kati ya ba-Israel na bikolo biye bibale.
26,9.	Tala Mit 16 na 17.
27,21.	Tala Bob 28,30+.
28,1.	O biteni bya 28 na 29 bazongeli mitindo mya milulu basili balimboli o eteni ya 23, kasi babakisi mwa ndimbola.
33,1.	Batangi awa nkombo 40 ya bisika ba-Israel balekaki banda babimaki o Ezipeti tee Kanana. Bobele ndambo ya nkombo ekomami mpe epai esusu, mpe nkombo isusu iye itangemi o Mit 21,16-19 ikomami awa te.
36,4.	Mobu mwa Lisabwi : tala Lv 25,23+.