1,1.	Likolo lifungwani, lokola ’te Nzambe alingi kokutana na bato.
1,2.	Ezalaki o 597 : Yeruzalem ato­mbokeli mokonzi wa Babilon ; oyo akangisi mokonzi Yoyakin o boombo ; mbele akangisi mpe Ezekiel na bankumu ba engumba elongo na ye. Mibu 8 na nsima, mpo bato ba Yeruzalem batomboki lisusu, ba-Babilon babebisi engumba mobimba, mpe bakangi bato ebele o boombo.
1,4.	Na limoni liye lya ‘likalo lya Yawe’ Ezekiel alakisi ’te Nzambe azali na nguya mpe na nkembo nsuka te ; asali yango na bilili biye bato ba ntango ena bazalaki koyeba, ata ayebi malamu ’te Nzambe azali elimo.
1,5.	‘Bikelamo binei’ biye, Ezekiel akobenga bango na nsima ‘bakerubim’ (9,3 ; 10,1-20 ; 13,20). Bilimo biye bizali o mboka Nzambe. Lelo tokotangaka bango ‘banzelu’. Na motango 4 profeta alakisi ’te babongi nye. Soko basaleli baye ba Nzambe baleki bato na nyama o makambo manso (10), Nzambemei akoleka bango naino mosika mpenza ! O buku ya Boyebisi Mibo­mbamo (4,6 ; 5,6 ; 6,1 ; 7,11 ;...) Yoane mpe aponi bilili biye bikomami awa.
1,26.	Ezekiel akoki kolakisa Nza­mbe te lokola azali, yango wana apesi ndakisa ya biye bikoki koulana na Nzambe, biye biso bato toyebi : moto, móto, etando ya likolo ; kasi Nzambe aleki bina binso mosika mpenza.
2,1.	Mbala soki monkama o buku eye Ezekiel amitangi ‘mwana wa moto’ : na maloba maye alakisi botau mpe boke bwa bato o miso ma Nzambe nta bokasi.
2,2.	Elimo eye ezali nguya eye Nza­mbe akopesaka bato baye atindi.
2,6.	Ata ezali nyama enene te, nkotó ekoki koboma moto soko eswi ye na mokila mozali na ngenge ; kofandela nkotó elakisi bongo kozala na likama lya liwa.
2,10.	Elakisi ’te buku eye etondi mpenza na makambo ma mawa.
3,12.	‘Elimo atomboli ngai’, lokola ’te : maye nakoloba mauti na mayele ma ngai te, kasi na Nzambe oyo atindi ngai.
3,15.	Tel-Abib (Babilon) : esika ba-Yuda basusu baye bakangemi o bo­ombo bafandi ; Kedar ezali nzela ya mai eye bato bapasolaki o esobe ya ekolo ena.
3,18.	Ezekiel atindami na Nzambe akebisa basumuki baye bakokutana na Nzambe ; etumbu ekokwela ye soko asali yango te.
3,22.	Toyebi te soko Ezekiel akomi mbubu to soko ye moko alingi koloba te. Mbele balakisi awa maye mako­kwela Yeruzalem ntango bakobotolo yango mbala ya ibale o 587. Profeta amikangi o ndako (lokola o engumba eye bayei kobundisa), akangemi (lokola moombo) mpe atiki koloba (lokola moto atondi na mawa).
4,1.	Na elili eye ayemi o biriki alakisi ’te bakobotolo Yeruzalem nsima ya mwa mikolo.
4,4.	Ezekiel akomi lokola moto akufa makolo mpo ya masumu ma bato ba Israel mpe ba Yuda. Toyebi te nini alakisi na mitango 390 na 40 : eleko ya masumu, ya boombo to ya etumbu eye bakozwa.
4,9.	Kolia biloko bisangani na mbuma ndenge na ndenge ezalaki ekila. Na bilei biye Ezekiel alakisi ’te bato bakei o boombo o mokili mwa bapagano bakozala na mbindo mpe na mpasi. Moto alei biloko bizali na bozito bwa mitako 20, alei bobele moke.
5,1.	Epai ya ba-Yuda bobele baombo na bato ba boloko bazalaki kokokolo nsuki. Ezekiel asakoli bongo lisusu ’te bakokende o boombo.
5,2.	Ezekiel asakoli ’te nsuka ya Yeruzalem ekomi pene : ndambo ya bato bakokufa na móto, basusu bakobomama na mompanga, basusu bakokende o boombo. Bobele mwa ndambo ya bato bakobika.
5,4.	Ntina ya molongo moye eyebani malamu te : mbele moto mosusu abakisi mwango na nsima.
5,9.	O bisika mingi bya Biblia ‘makambo ma nsomo’ elakisi bokumisi bikeko.
5,16.	Ntango bato bazalaki kotia mitema na ye, Nzambe abikisaki bango, mbala esusu na bosali makamwa manene ; kasi sikawa akotumbola bango mpo ya masumu ma bango.
6,1.	Batombeli ngomba iye bobe, mpo ba-Kanana mpe ba-Israel batongaki ndako ya banzambe ba lokuta wana.
6,8.	Se lokola baprofeta liboso lya ye, Ezekiel asakoli ’te Nzambe akoyokela ndambo ya bato mawa, mpe akobikisa bango soko bakobongola mitema. Tala Am 3,12 ; 5,15 ; Mi 2,12-13 ; 5,2 ; Iz 4,2-3 ; 10,19-21, mpe Yeremia o bisika bisusu.
6,14.	Banda eliki (o Sudi) kin’o Ribla (o ngambo ya ndelo ya Nordi), lokola ’te o ekolo mobimba, bato bakofanda lisusu te.
7,7.	Mokolo (Yawe akotumbola bato ba ye mpo ya masumu ma bango) mobelemi.
7,14.	Ntango monguna akobeleme, bakobete mondule mpo ya kobenga basoda ba engumba, nzokande ekozala se mpamba, mpo etumbu ekokwela bango.
7,18.	Bato bazalaki kosala lilaka bazalaki kokatisa nsuki mpe kolata ngoto.
8,1.	Ezalaki o 692, mobu mwa motoba nsima ya bokei o boombo (tala 1,2).
8,2.	Tala 1,26+.
8,3.	O limoni Ezekiel akomi o Te­mpelo ya Yeruzalem, mpe amoni ekeko eye bato bazalaki kokumisa ; yango eyokisi Yawe nkanda na zuwa. Ekoki kozala ekeko ya Astarte, eye Manase akotisaki o Tempelo (2 Bak 21,7) mpe mokonzi Yozia alongolaki na nsima (2 Bak 23,4).
8,11.	Awa bakonzi ba ekolo bazalaki kokamba milulu mya losambo ; o Israel mizalaki se kokambama na banga­nga Nzambe. Bazalaki mpe kokumbamela Nzambe wa solo te, kasi bikeko ndenge na ndenge biye bilakisi nyama mbindo.
8,14.	Tamuz azalaki nzambe wa lokuta oyo abotisaki mabelé mbuma mingi. O nsuka ya mobu, ntango mpio eyei makasi mpe mabelé makomi kozanga bilona, balobaki ’te Tamuz akufi, bongo baleli ye. Ntango molunge moyei lisusu, balobaki ’te asekwi.
8,16.	O Babilon bazalaki kokumbamela mwese, sanza na minzoto ; ba-Yuda basusu babandi komekola bango.
8,17.	Toyebi momeseno moye mwa bapagano malamu te.
9,2.	Nzambe atindi babali baye motoba mpo ’te baboma bato babe ; oyo wa nsambo azalaki mokomi oyo ase­ngelaki kotia elembo o bilongi bya baye bakobika (9,4). Tala Boy 7,2-3.
9,3.	Tala 1,5.
9,8.	Tala 6,8.
10,1.	Awa tokotanga limoni lisusu lya ‘likalo lya Yawe’ (1,24-28).
10,2.	Basili babomi bato ebele, sikawa basangeli ’te engumba mobimba ekotu­mbama.
10,3.	O Biblia limpata lizali elembo ’te Nzambe azalaki wana (Bob 13,21 ; 14,19 ; Mit 9,15 ; 1 Bak 8,10 ;...).
10,18.	Ezekiel ayebisi ’te Nzambe ali­ngi kotikala o Tempelo te, mpo bakomisi yango mbindo na bikeko ; ali­ngi mpe kotikala o ntei ya bato baye te, mpo baboyi kotosa ye. O 11,22-23 tokotanga ’te Nzambe alongwi o Yeruzalem.
11,11.	Bayangeli basusu ba Yeruzalem bakangemaki o boombo te. Bakanisi ’te likambo lyoko likoki kokwela bango te ; bongo balingi komitongela ndako inene ya sika, yango wana bazalaki koloba eyele eye : miloli mikoki kosala misuni (bango moko) o kati ya nzungu (Yeruzalem) eloko te. Kasi Ezekiel azongiseli bango : Ezali bongo te ! Bibembe bya baye babomami bizali lokola misuni o lisasu, mpe bakobwaka bino mosika o libanda lya engu­mba !
11,17.	Nzambe azali lokola bato te, baye bapusi komikanisa : akolimbisa bato bakangemi o boombo ; na motema nta bolingi alakeli bango bongo. Ekozala ‘bondeko bwa sika’, lokola ekomami o Yer 31,31-34, ata Ezekiel ata­ngi yango bongo te.
11,22.	Tala 1,18.
12,1.	Ezekiel akebisi bato ba Yeruzalem ’te bakokende o boombo ; mpo ’te bayoka ye malamu alakisi maye mako­kwela moto akei o boombo : ndenge akokende motema likolo, mpe bobele na mwa biloko.
12,21.	Awa amoni ’te bato batii nte­mbe na likebisi lya ye, mpe balobi polele ’te yango ekosalema te, Ezekiel asakoli ’te mpasi eye ekokwela bango noki.
13,10.	Baprofeta ba lokuta bakokitisa mitema mya bato na maloba masepelisi bango, mpe babangi koyebisa bango ’te basengeli kobongola mitema ; se lokola moto abongisi efelo malamu te : apakoli bobele mokobo o bisika ebebi, kasi mbeba etikali.
13,18.	Basi baye bazalaki kolatisa bato biteni bya bilamba, mpe komikumisa ’te bayebi makambo makoya.
14,4.	Soko moto oyo akokumbamelaka banzambe ba lokuta ayei kotuna Yawe, eyano Yawemei akopesa ye, nde etumbu akozwa mpo aboyi kokumbamela Nzambe wa solo mpe azali moto wa bokosi.
14,12.	Awa (mpe o 18 na 33,10-20) Ezekiel asakoli polele ’te moto moko akozwa mbano to etumbu engebene na misala mya ye : ata bato ba bosembo batikali mingi o engumba te, etumbu ekoki kokwela engumba mobimba te. Apesi ndakisa ya yango : Nowe (Lib 6,9), Yob na Danel, oyo atangemi o Biblia te, nzokande ayebanaki mingi o bikolo biike.
15,1.	Ezekiel ateyi awa te ’te nzete ya vino esengeli kobota mbuma ilamu (lokola o Iz 5,1-7), kasi soko mobimbi na bitapi bya yango bizali malamu te, ekoki kozala na litomba lyoko te : bakotumba yango. Se bongo na ekolo ya Nzambe.
16,1.	Lokola o bisika mingi bya Biblia, balakisi ekolo ya Israel lokola elenge mwasi oyo Nzambe apusi kolinga. Kasi ntango Nzambe abali ye, mwasi oyo akimi ye, yango wana ase­ngeli kopesa ye etumbu mpo akosi ye. Nzokande alingi bobele ’te abongola motema mpe azongela ye.
16,3.	Ezekiel alingi kolakisa ’te ba-Israel banso bazalaki bankoko ba Abarama te, mpo bato ba bikolo bisusu basangani na bango.
16,4.	O ntango ena bameseneki kosala bongo na bana babotami sika ; bakanisaki ’te mongwa mozalaki kole­ndisa nzoto ya bango ; se bongo, ekolo ya Israel ezangaki makasi ntango ebotami, ezalaki mpe kitoko te ; Nzambe akolingaka Israel bobele mpo ye moko aponi yango.
16,12.	O eleko ena basi bazalaki kolata mpete ya wolo o matoi mpe o zolo.
16,15.	O Biblia bazalaki kotala moto oyo azalaki kokumbamela banzambe ba mpamba lokola moto asali bo­ndumba, mpo bazalaki komekola makambo ma bikolo bisusu.
16,20.	Ntango esusu babonzelaki Nzambe bana ba bango (tala 20,26 ; 23,37 ; Yer 32,35 ; Lv 18,21).
16,26.	Mpo bakoka kotia mitema na bango, o esika ’te batia elikya na Nzambe.
16,37.	Bikolo biye Israel alukaki mpo ’te akata na bango bondeko, biye bibe­ndaki Israel mpe o nzela ya mimeseno mya bango, bobele bikolo biye bakoyokana mpo ya kobundisa ye.
16,44.	Liboso ’te Davidi abotolo Yeruzalem mpe akomisa yango engumba esantu, bapagano (ba-Yebus) bazalaki kofanda wana. Na nsima Yeruzalem ekomi lisusu lokola o ntango ya ba-­Yebus, ba-Iti na ba-Amor, mpo bato ba ye bazongeli makambo ma bapagano. Balandaki bongo ndakisa ebe ya bato ba Samaria mpe ba Sodoma. Ya­ngo wana banguna batu­mbaki yango.
16,46.	Bingumba na mboka iye izi­ngi Yeruzalem.
17,1.	O ntango ena bazalaki kolakisa mokonzi wa ekolo na elili ya nyama to ya nzete. Awa balakisi bongo Nabukodonozor lokola engondo, ndeke eye eleki ndeke inso na makasi ; engondo ya ibale, nde mokonzi wa Ezipeti.
	- Na elanga ya vino balakisi Israel, nsonge ya sédere, nde mokonzi na bankumu baye bakokende o boombo. Tala ndimbola ya yango o 17,11-18.
18,2.	Eyele eye elakisi maye bato bazalaki kokanisa : bana bazwa nde etu­mbu mpo ya mabe ma baboti ? Ata o ntango ya Yezu bato basusu bazalaki naino na makanisi mana (Yo 9,2). Moko wa baye basakoli ’te ezali bongo te, nde Ezekiel : Nzambe akotala bobele malamu na mabe ma moto moko moko. Kasi toyeba ’te o ntango ya Ezekiel Nzambe ayebisi bato naino makambo ma nsekwa mpe ma bomoi bwa lobiko te. Yango wana bakobimisa motuna mosusu : Soko ezali ya solo ’te moto mabe akozwa etumbu, ndenge nini Nzambe akoki kolingisa ’te basemba bayoka mpasi, ata bomoi bwa bango mobimba, mpe bato babe bazwa bolamu ? (Tala Yob 4,7).
18,8.	Ata emononi ’te kodefisa mosolo mpo ya kozwa mbakisa epekisamaki (Bob 22,24 ; Lv 25,36-37 ; Mbk 23,20), bato badefaki bazalaki kopesa mwa mbakisa, kasi kosenge mbakisa na moto azalaki kokelela epekisamaki makasi.
18,23.	Mbele molongo moye (molakisi motema nta ngolu mwa Nzambe) moleki bikoma binso bya Bondeko bwa Kala na bonzenga.
18,25.	Mbele bato basusu, baye bazalaki komimono bato ba bosembo, bakokaki kondima te ’te Nzambe akoli­mbisaka oyo ayoki motema mawa, mpo balingaki ’te Nzambe atumbola ye !
19,1.	O nzembo eye ya bomileli batangi mpasi iye ikweli bakonzi ba nsuka ba Nordi : o 609 bakambaki Yoakaz (2 Bak 23,33) mpe o 597 Yoakin (2 Bak 24,12-16) o boombo.
19,2.	Nkosi mwasi ezalaki elembo ya ekolo ya Yuda ; mwana wa yango, nde mokonzi wa Yuda.
19,10.	Awa lisusu Ezekiel apesi moulani mwa elanga ya vino mpo ya kolakisa Israel (15,1... ; 7,6...).
20,1.	O 591 liboso lya Y.K., mibu mibale liboso ’te bato ba Babilon babebisa Yeruzalem mobimba.
20,25.	Mibeko mya Nzambe miko­bo­ngela moto oyo atosi myango, kasi mikozalela bato bakinoli myango libaku. Yango wana, lokola bato ba eleko ya ye bazalaki koloba, Ezekiel akomi ‘Nzambe alingaki bongo’.
20,35.	Bakokende na bango o ekolo ya bampaya, esika bakoyoka mpasi lokola o ntango bazalaki eliki.
21,3.	Yuda, eteni ya Sudi ya Kanana, ezalaki na zamba mingi ; Yuda ekobeba bongo lokola zamba etumbami.
21,8.	Ata ebongoki te ’te etumbu ekwe­la moto semba, makambo ma mpasi makokwela bato banso.
21,23.	Mokonzi wa Babilon ayebi te soko akobundisa liboso Raba (ya ba-Amon) to Yeruzalem. Yango wana atuni banzambe ba ye na bobeti zeke ; zeke elakisi Yeruzalem.
21,26.	Terafim ezalaki ekeko eke ya banzambe, eye bakokaki komeme o mobembo.
21,33.	Nsuka ya Yeruzalem ebelemi ; ba-Amon bakanisi ’te bazalaki na likama lisusu te, kasi bango mpe bakozwa etumbu.
22,1.	Profeta alakisi mabe manso ba-Israel bazalaki kosala ; liboso (o 16 na 20) atangaki masumu ma bankoko.
23,1.	O eteni ya 16 Ezekiel asili alakisi Israel lokola mwasi oyo akosi mobali wa ye ; sikawa apesi moulani mosusu : bandeko basi babale, Samaria na Yeruzalem, baye balekisi ndelo na bosali bondumba.
23,4.	Nkombo Okola na Okoliba izali mbokela : Samaria (Okola) ato­ngaki Tempelo eye Nzambe alingaki te, mpe Yeruzalem (Okoliba) atongaki Tempelo eye Nzambe atali lokola Tempelo ya ye. Bondumba boye basali, nde bokumbameli banzambe ba lokuta ba bikolo bisusu (Ezipeti, Asur, Babilon eye etangemi Kaldea awa).
23,32.	Komelisa moto nkeni ya vino bololo lokola ’te kopesa ye etumbu ; nkeni eye ekolangwisa ye mpe ekobimisela ye makambo (tala Iz 51,17 ; Yer 25,15 ; 49,12).
23,40.	Bayangeli ba Yeruzalem batindi bantoma bakende wapi ? Ntina nini ? Ata boni, Ezekiel ateyi ’te base­ngeli kotia mitema se na Nzambe.
24,1.	Yango esalemi o mokolo bato ba Babilon bayei kozinga Yeruzalem (2 Bak 25,1 ; Yer 52,4) ; o ntango ena Ezekiel azalaki o Babilon.
24,3.	Ata o ebandela nzungu ekobatela misuni, mosika te mikolambama malamu (tala mpe 11,3).
24,6.	Soko nzungu egugi mingi te, móto mokoki kolongola bogugi ; kasi bakoki kopetola Yeruzalem lisusu te mpo bogugi boleki mingi ; bakobebisa yango.
24,15.	Mwasi wa Ezekiel akokufa na mbalakaka, nzokande Nzambe apekisi ye kosala lilaka, mpo mosika te bato ebele bakokufa o Yeruzalem, bongo bakozala na ntango ya kolela bawei lisusu te.
25,1.	Lokola o buku ya baprofeta basusu, awa mpe basangisi maloba ma bokaneli bikolo bisusu. Profeta akokanela ekolo yoko yoko mpo ya mabe mpe mafinga ma bango.
26,1.	Tiro na Sidoni izalaki bingu­mba binene bya Fenisia. Tiro ezalaki engumba ya bato ba mombongo, eye etongami o esanga pene na libongo (yango wana balakisi yango lokola masuwa). Lokola Yeruzalem, bato ba Tiro bandimi bokonzi bwa Babilon te, kasi ntango babotoli Yeruzalem, bato ba Tiro basepeli. Ezekiel ayebisi bango ’te bango mpe bakosuka. Mpe ya solo, bato ba Babilon bakobundisa bango mibu 13, mpe bakokomisa bango babola, ata bakokaki kobotolo engumba ya bango te.
33,22.	Lokola o 3,26-27 mpe o 24,27, awa mpe Ezekiel akokaki koloba te mwa mikolo, mpo ya malali to, lokola ye moko akanisi, ‘mpo Nzambe ali­ngaki bongo’.
33,24.	Baye babiki o liwa mpe o boombo bakanisi ’te bakoki kofanda o mokili moye bango balingi ; kasi Ezekiel akebisi bango : soko bokobi kosala mabe, bokozala bankolo ba mokili moye te.
34,1.	Ezekiel asiliki na bomoni ’te bayangeli bazalaki kopangana na bolamu bwa bato te, mpo bazalaki kobakisela bato ba mawa mpasi esusu : bayeba ’te Nzambe moto akotuna bango !
34,11.	Nzambe alakeli bato ba ye ’te ye moko akozala mokengeli wa bango, mpe akopesa bango nkoko wa Davidi oyo akokamba bango o nkombo ya ye (34,23). Ekozala eleko ya bondeko bwa sika : bato bakokelela te, banso bakofanda na boboto (25-31). Biso toyebi ’te Yezu amiyebisi bo mokengeli malamu (Yo 10,11-16), oyo akoluka mpata eye ebu­ngami (Mt 18,12-14).
34,17.	Tala Mt 25,32-34.
35,1.	Bato ba Edom (Seir) basepelaki ntango banguna babebisi Yeruzalem o 587. O 35,10 ekomami ’te ba-Edom balingaki kobotolo biteni bibale bya Israel mpe bya Yuda ; kasi bakokaki kofa­nda bobele o eteni ya Sudi ya Yuda.
36,1.	Bato ba bikolo biye bizalaki kofanda pene na bango bayaki kobotolo biloko bya bato ba Yeruzalem nta­ngo engumba ya bango ebebaki (36,5) ; yango wana etumbu ekokwela bango. Ezekiel asakoli ’te bato bakangemi o boombo bakozonga mpe bakobongisa mokili (36,8-38).
37,1.	Bato bakangemi o boombo o Babilon balembi ; bazalaki na elikya ya libiki lisusu te (37,12). O limoni liye, lizali lokola moulani, Ezekiel atindami kosa­ngela bango nsango ya elikya : Nzambe akoki kosala ’te bato bawei bakoma na bomoi lisusu ; akosala ya­ngo mpo ya ekolo ya ye.
37,15.	O 931 ekolo eye Davidi na Salomo babongisaki ekabwani na biteni bibale ; baprofeta batalaki bokabwani boye bo likambo lya mpasi enene. Ezekiel ayebisi awa ’te Israel na Yuda bikosangana lisusu ; yango ekosalema na Davidi wa sika (37,24). O mibu mya nsima ba-Yuda bakokoma na bonsomi mpe na boyokani, solo, kasi oyo ayei kosangisa bato banso o libota lyoko ekozala Yezu Kristu, nkoko wa Davidi mpe Mwana wa Nzambe.
38,1.	Kotongolo ntina ya maye makomami o 38-39 ezali kwokoso. Toyebi mokili moye mwa Magog mpe mokonzi Og te. Mbele Ezekiel alingaki kolakisa na nkombo iye banguna ba­nso ba Israel, baye bakouta o Nordi. Tokoki kotala maloba maye lokola liteya litali makambo ma nsuka ya molóngó : bato ba ekolo eye Nzambe aponi basengeli kobunda na mpasi ndenge na ndenge liboso ’te Nzambe andimela bango seko (39,21-29). Santu Yoane akotanga nkombo iye, Gog na Magog, (Boy 20,7-9 ; 16,16) mpo ya kolakisa banguna baye bakoluka Eklezya nta­ngo inso na makambo.
40,1.	Banda eteni eye ya 40 tee nsuka ya buku mobimba, Ezekiel alimboli maye makokwela bato bakouta o boombo mpe bakozonga o ekolo ya ba­ngo. Bobele Nzambe moto akoyangela Israel, mokonzi wa ekolo akozala se mosaleli wa ye. Nzambe akokamba bango o makambo manso, yango elakisami na ndako ya ye (Tempelo) eye ezali o kati ya ekolo. Ezekiel alakisi Tempelo eye (40,1 - 43,12) na biteni bya yango binso. Lokola o limoni asili amoni altare ya sika, biyenga na mabonza : binso bikobongo nye mpo ya kopesa Nzambe lokumu lokoki na ye (43,13 - 46,24). Amoni mpe ndenge bakokabolela mabota manso mokili : eteni eye libota lyoko lyoko likozwa ekozala se ndenge yoko. Ya solo, ekoki kosalema bongo te, kasi Ezekiel alingi se kolakisa ’te ebongi ’te mokili mozala na boyokani, na boboto mpe na bosembo mpo ’te bato banso bazala na bolamu bwa solo ; mokili mwa bato batosi Nzambe mokoki kozala bongo. Mpo ya kobongola manso o biteni biye (40-47) malamu ezalaki mpasi, mpo mbala mwa mingi tokoki koyeba makanisi ma mokomi malamu te, mpe mpo ya mosala mwa bato baakisi maloba maye ma Ezekiel. Yango wana mpe tokokaki kopesa maloba ma ndimbola lisusu te.