1,2.	Bato banso bamoni mpasi enene eye ekweli ekolo. Ata babange bamoni bongo naino te.
1,4.	Mapalela ndenge na ndenge mayei kosilisa mbuma inso o bilanga ; nzala enene ekongala o ekolo.
1,6.	Bato ba ekolo esusu : mapalela ebele lokola limpinga lya basoda.
1,8.	Profeta azali koloba awa na bato babyangemi mpo ’te bamilela o miso ma Nzambe.
1,9.	Elongo na mpata eye bazalaki kobonza ntongo mpe mpokwa inso o Tempelo ya Yawe, bazalaki kobonza fufu ya mbuma ya mampa, mafuta na vino. Awa biloko biye bikomi koza­nga, bakoki kobonza mabonza ma mokolo na mokolo lisusu te lokola etindami (Bob 29,38-42 ; Mit 28,3-8).
1,13.	Bazalaki kolata ngoto bo ele­mbo ya lilaka.
1,15.	Mpasi eye mapalela mayokisi elakisi ’te Nzambe alingi akoto o ma­ka­mbo ma bato. Baprofeta bakotangaka mokolo mwango ‘mokolo mwa Yawe’. Nzambe akoki kondima te ’te bato baike bakoba kosala masumu o Israel mpe o bikolo bisusu ntango basemba na bato batau bazalaki kobunda na mpasi ; akotumbola bato babe mpe akopesa basemba mbano ekoki na ba­ngo. Mbala inso likambo linene lizalaki kokwela ekolo, balobaki ’te ezalaki mokolo mwa Yawe. Kasi Mokolo monene mwa Yawe mokoya o nsuka ya molóngó, ntango Nzambe akokata ma­kambo ma bato banso (Yl 2,1 ; 4,14 ; Am 2,16 ; 5,18 ; Sof 1,7 ; Abd 15 ; Ez 30, 3). O miso ma bakristu mokolo mona mokozala mpe mokolo mwa ‘boyei bwa Kristu’ (Bik 2,20 ; 1 Kor 1,8 ; 2 Kor 1,14 ; 1 Tes 5,2 ; 2 Tes 2,2 ; 2 Pe 3,10).
1,19.	Ntango mpasi enene ya mapalela ekokwela ekolo, mbula ekonoko mpe te, bongo biloko binso bikokauka.
2,2.	Tala 1,4+.
2,13.	Yoel ateyi ’te bato basengeli kobongola mitema, soki te ná losambo ná mabonza ma bango makozala se mpamba.
2,15-17.	Tala 1,13-14.
2,27.	Nzambe alakeli bango bongo bobele soko bakoboya ye te.
3,1.	Bato banso bakobondela nko­mbo ya Yawe bakozwa nguya ya komibongisa mpe ya kosakola o nkombo ya ye (Mit 11,29 ; Ez 11,19-20 ; 36,26-27). O mokolo mwa Pentekoste Petro asakoli ’te likambo lyango lisalemi na boyei bwa Elimo Santu (Bik 2,17-25).
3,3.	Makambo maye mazali se bile­mbo ; na bilembo biye bazalaki kolakisa ’te Nzambe akoya na nguya enene.
4,4.	Tiro, Sidoni na ba-Filisti bafu­ndami awa mpo basalaki Yeruzalem mabe mingi. Mokomi akanisi awa liboso ndenge bato bakangemaki o boo­mbo (o 597 na 587, mpe mbele mbala isusu na nsima). Bobele mpasi eye bayokisi ba-Israel ekokwela bango.
4,9-14.	Profeta abyangi bato ba bikolo binso bameka kokoka na Yawe : ata na bibuneli nini ata na basoda boni bakokoka ye te. Mokolo Nzambe akosambisa bango banso, ekosalema na bango lokola bakokataka matiti ; bakonyata bango lokola mbuma ya vino o ekamoli.
4,18.	Nsima ya bosakoli bitumbu bikokwela bato baboya Nzambe, profeta asakoli ’te Nzambe akokabela bato bandimi ye esengo enene ; akomi yango na bilili biye bato ba lelo bayebi lisusu te, kasi liteya liye lizali polele : mokolo moko Nzambe akokokisa malamu maye akani kosala bato ; mpe moto moko te akokoka kopekisa ye. Baprofeta bapesi ndakisa ya bolamu mwa mokili, kasi Yezu Kristu akolakisa ’te Nzambe alengeleli bato bomoi bwa seko bokozala na esengo nsuka te.

