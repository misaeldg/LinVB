1,5.	Adonia, mwana wa Davidi, oyo akomaki mwana wa yambo ut’o liwa lya Amnon mpe lya Absalom, akoki kokitana Davidi o ngwende. Lokola Absalom asalaki ntango alengeleki botomboki bwa ye, oyo mpe aluki kokoma na basaleli mingi.
1,35.	Davidi alakisi Salomo bo mokitani wa ye, mpe apesi ye makoki ma bokonzi na milulu miye atindi.
1,50.	Matumu manei ma altare matombwami lokola maseke (Bob 27,2). Moto asimbi liseke lya altare azalaki kosenge bobateli bwa Nzambe o ntango balingi koboma ye (tala mpe 2,28).
2,4.	Elako ya bobenisi eye Natan ayebisi (2 Sam 7,11...).
2,6.	Davidi akokaki kosukisa Yoab te, yango wana atindi mwana wa ye Salomo atumbola ye. Se bongo atindi Salomo azongisela Simei mabe ma mafinga ma ye (2 Sam 16,5-13 ; 19,17-24), mpo asilaki kolaya ’te ye moko akozongisela ye mabe te. Davidi, ata asalaki makambo mazweli ye lokumu, alakisi awa ’te ayebi naino kolimbisa te.
3,5.	Salomo, awa asilisi kobonzela Nzambe, alali o ndako esantu na elikya ’te Nzambe ayebisa ye, na maloba to na ndoto, maye ye alingi.
3,26.	Likambo liye Salomo asili ayebaki na bwanya bwa ye likosalema : mama wa solo akoki kolinga te ’te baboma mwana wa ye ; yango wana andimi kotikela mwasi mosusu mwana.
4,19.	Ezali ekolo ya Yuda : bato ba libota liye batindamaki kofuta mpako enene lokola mabota masusu te ; yango ekosala ’te basusu bakoyokela bango zuwa.
5,11.	Etan na bato basusu basato batangemi awa bazalaki bato ba bwanya baye bayebani mingi na ba-Israel banso.
5,20.	Molongo moye mwa ngomba, o eteni ya Nordi ya Kanana, motondaki na nzete ya sédere kitoko ebele ; izalaki kokola bobele o esika eye. Mabaya ma ya­ngo mabongoki mingi na mosala mwa botongi ndako.
6,2.	O ntango ena ‘bolai bwa maboko mabale’ bokokaki na metele yoko. Te­mpelo eye ezalaki enene mpenza te, kasi ekembamaki na biloko ebele ya motuya. Ezalaki bobele na ezibeli yoko mpo ya kokoto, mpe na biteni bibale : o eteni ya liboso (Ekal : 20m/10m) nganga Nzambe azalaki kobonzela Nzambe mpaka ya malasi ntongo mpe mpokwa inso. Ase-ngelaki kokengele ’te mwinda nsambo o etemiseli izima te. O eteni ya bibale (Debir : 10m/10m), esika Sanduku ya Bondeko etiamaki (8,6), bobele nganga mokonzi akokaki kokoto mbala yoko o mobu mobimba (Lv 16,2-3 ; 12-16).
6,23.	Awa batangi bakerubim 2 bya wolo te, baye batiamaki o ezipeli ya Sa­nduku (Bob 25,18), kasi bikeko binene 2 bizipamaki na wolo. Kerubim azalaki elimo eye Nzambe akeli mpo ’te asalela Ye.
7,2.	Motondo mwa ndako eye enene mopikamaki o makonzi ebele ma mabaya ma sédere, yango wana bazalaki kotanga ndako eye ‘zamba lya Libano’.
7,23.	Ebombelo eye enene mpenza ya mai ekokaki kokwa litere 45 000. Ba­nganga Nzambe basengelaki komipetola wana (Bob 30,18-21 ; 2 Mkl 4,6).
8,10.	Londende lozalaki kolakisa ’te Nzambe azalaki wana. Tala mpe Bob 16,10 ; 19,16 ; Mit 9,15-22.
8,15.	Salomo akundoleli bato bilako biye Natan atangaki (2 Sam 7,4-16).
8,21.	O mabanga manene 2, maye matiami o Sanduku, mibeko mya Nzambe mikomami (1 Bak 8,9 ; Bob 34,1-4 ; 27-29).
8,27.	Ata akomilakisaka o Tempelo, Nzambe akokangemaka o ndako te ! Ye aleki nse na likolo na bonene : eloko yoko ekobombamaka o miso ma ye te.
8,46.	Eteni eye ya bosengi bolimbisi mpo ya bato bakendeki o boombo ebakisami na losambo loye la Salomo, nsima ya nsuka ya Yeruzalem (587).
9,22.	Ata bampaya batindamaki kosalela bango mosala, ba-Israel bango moko basengelaki kopesa bango maboko o misala miye mokonzi atindaki (5,27 ; 11,28). Yango wana bakomilela na nsima (12,3).
10,1.	Saba ezalaki ekolo eke o Arabia ya Sudi.
10,16.	Nguba enene ezalaki kobatela nzoto mobimba ; nguba eke ezalaki kobatela bobele eteni ya nzoto, kasi mpo ya kotombola to kokitisa yango ezalaki mpasi mingi te.
11,1.	Touti kotanga mambi ma lokumu la Salomo. Banda eteni eye mokomi akoyebisa biso mpe botau mpe mbeba ya ye : abalaki basi bampaya mingi, baye bakobenda ye o nzela ya bokumbameli bikeko bya banzambe ba bango. Salomo atongeli banzambe baye ndako isantu penepene na Tempelo ya Yawe (11,7) !
11,15.	Tala 2 Sam 8,13-14.
11,28.	Mabota ma Efraim mpe ma Manase.
11,30.	Yeroboam azwi biteni 10, biye bilakisi mabota 10 ma Nordi. Roboam akotikala bobele na biteni 2 bilakisi mabota ma Yuda mpe ma Simeon. Kasi o ntango ena bazalaki bobele kotanga libota lya Yuda, mpo ba-Simeon mingi basili kokoma bato ba libota lya Yuda.
11,40.	Mbele Salomo ayokaki ’te Yeroboam alingi kokoma mokonzi.
12,1.	Na ‘Israel mobimba’ balakisi mbala mingi mabota 10. Soko Roboam azalaki na lolendo te mpe alukaki boyokani bwa solo, mbele mabota ma Nordi malingaki kondima ye bo mokonzi.
12,4.	Mpo ya misala mingi mya boto-ngi Salomo afutisi bato mpako mingi. Yango eyokisi mabota ma Nordi mpasi mingi, leka libota lya Yuda.
12,29.	Betel ezalaki o nzela ut’o Nordi kin’o Yeruzalem. O esika eye ndako esantu ezalaki banda ntango ya Abarama (Lib 12,8). Dan, eye ezalaki o eteni ya Nordi ya ekolo, ezalaki mpe na ndako esantu ut’o ntango ya bazuzi (Baz 17-18).
12,32.	O ndako isantu iye ba-Kanana bazalaka kokumisa banzambe ba bango kala. O bisika bisusu babandaki kokumisa Yawe. Yeroboam akamati banganga ba banzambe ba lokuta mpo akomisa bango banganga Nzambe ba Yawe (tala mpe 13,33). O 2 Mkl 11,13-17 emononi ’te ba-Levi baike, baye bafandaki o Nordi, balongwi wana mpo bakende epai ya Roboam o Yeruzalem.
13,2.	Tala 2 Bak 23,15-16.
14,2.	Tala 1 Bak 11,29-40.
14,11.	Moto moko te akomeka koku­nda bibembe biye bikoliama na ndeke mpe na mbwa. Kokundama te ezalaki nsuka ya bitumbu.
14,14.	Mokonzi oyo, nde Basa : akotombokela Nadab, mwana wa Yeroboam, o nsuka ya mobu mwa mibale mwa bokonzi bwa ye (15,25-29).
14,17.	Tirsa ezalaki mboka-mokonzi ya liboso ya Israel, liboso ’te Samaria etongama (16,24).
14,19.	Bakonzi ba Israel mpe ba Yuda bakomisaki mambi manene ma boya-ngeli bwa bango o Buku ya Bikela. Moto akomi buku eye ayebisi mbala mingi ’te atangaki buku yango (14,29 ; 15,7 ; 15,23 ; ...).
15,26.	Lisumu lya Yeroboam likomami mbala mingi o buku Bakonzi : aka­bwanaki na libota lya Davidi, mpe abe­ndaki mabota 10 ma Nordi o nzela ya bokumisi Yawe oyo alakisami na ekeko ya wolo ya ngombe. Esalemaki o Betel mpe o Dan, bisika biye Yawe aponoki te.
15,29.	Tala 14,10-16.
16,24.	Samaria ezali mboka-mokonzi ya sika ya Israel ; o 721 ba-Asur bakoboma yango (2 Bak 17,5-6). Bakotonga ya­ngo lisusu o ntango ya Yezu bo mboka-mokonzi ya Samaria. Toyebi ’te ba-Yuda, o ntango ya Yezu, bazalaki kolinga ba-Samaria te, mpo ekolo ya bango eto­ndaki na bampaya.
16,34.	Tala Yoz 6,26.
17,1.	Awa babandi lisolo lya Nkombo ya Elia lokola ’te ‘Nzambe wa ngai, se Yawe’. Elombe oyo azalaki bobele ye moko o etumba na banganga Nzambe ba Baal mpe na baye bazalaki kokumisa bikeko o ekolo ya Nordi. Azalaki moto wa boyambi mpenza. Nzambe akondimela ye ntango akosenge ye bilembo na makamwisi. Ntango Elia akolimwa (akokufa te), Elizeo akokoba mosala mwa ye.
17,18.	Mama oyo akanisi ’te Nzambe atumboli ye na liwa lya mwana wa ye, kasi na nsima akondima ’te Yawe moto atindeli ye profeta Elia mpo ayamba ye... mpe asekwisa mwana wa ye.
17,21.	Mpo ya koyoka ntina ya lisolo liye malamu, bakoki kotanga likamwisi liye Elizeo asalaki (2 Bak 4,32-35).
17,24.	Mama oyo afandaki o ekolo ya bapagano, baye bakumisaki banzambe mingi. Na nsima andimi ’te Nzambe wa Elia, Yawe, azali Nzambe wa solo se moko (17,12).
18,4.	Yezabel, mwana wa mokonzi wa Sidoni, Itobaal, azalaki mwasi wa mokonzi Akab (16,31). Asalaki manso mpo ya kokotisa bato o nzela ya bokumisi banzambe Baal mpe Asera. O buku eye bayebisi te ’te abomisaki baprofeta ba Yawe.
18,17.	Elia ayebisi Mokonzi : moto asali ’te mpasi ekwela ekolo ezali profeta te oyo asakoli yango, kasi oyo atumolaki ye na masumu ma ye.
18,21.	Elia apaleli bato mpo bazalaki lokola mai ma bwato : lelo bazalaki kokumisa Baal, lobi Yawe..., nzokande ba­ngo babale bakoki kozala Nzambe wa solo se moko te. Yango wana akokata mondenge na banganga Nzambe ba Baal : Nzambe wa solo akomilakisa na nguya ya ye !
18,40.	Ata Elia ayebi naino te ’te likambo lya koboma bato likoki kosepelisa Nzambe te.
18,45.	Yizreel ezalaki o Esti ya ngo­mba Karmel. Bobele Samaria elekaki yango na bonene.
19,2.	Yezabel alingaki kokimisa Elia ; alingaki koboma ye te, mpo abangaki bato.
19,4.	Elia azalaki na elikya ’te moko­nzi na bato bakozongela Yawe, mpo ya likamwisi lisalemi o ngomba Karmel. Kasi se mpamba, yango elembisi ye.
19,8.	Oreb ezali nkombo esusu bakopesaka Sinai. Elia akei o esika Nzambe abimelaki bankoko mpe akataki na ba­ngo bondeko, mpo ’te Nzambe alendisa ye.
19,13.	O mobenga moye Moze abo-mbamaki ntango Nzambe abimelaki ye (Bob 33,22).
19,17.	Eloko yoko esalemi te ya maye tokoki kotanga o lisolo lya Elizeo.
19,19.	Mpo ya kolakisa ’te azalaki nkolo wa eloko to wa moto, bazalaki kozipa yango (ye) na monkoto (Rute 3,9). Elia alakisi bongo ’te akamati Elizeo o mosala mwa ye.
20,1.	Damasko ezalaki mboka-mokonzi ya ekolo ya ba-Aram, o Nordi-Esti ya Israel. Banda ntango ya Elizeo mpe ya mokonzi Akab, ba-Aram bakobundisa ba-Israel mpo ya koyangela bango. Tee ntango ba-Asur bakosukisa bango (732), ba-Aram bazalaki kokanela ba-Israel.
21,9.	Ntango likambo lya mpasi li­kwe­laki bato (bokono to nzala), bazalaki kotinda bato balala nzala mpe basambela losambo la lisanga mpo bakitisa motema mwa Nzambe, mpe bayeba nani asalaki mabe maye mabimisi likambo lyango. Mokosi Yezabel atindi ’te Nabot afanda o esika ya lokumu mpo ’te amono mabe te.
21,16.	Moto oyo bakateli ye etumbu ya liwa, babotoli ye biloko binso.
21,19.	Elia alobi na mpiko mpenza, lokola Natan alobaki na Davidi : alakisi ye mabe ma ye manene polele.
21,29.	Maye malobami awa makosalema solo (2 Bak 9-10).
22,7.	Mokonzi Yozafat atii ntembe na baprofeta, mpo na maye bazalaki kosakola o miso ma mokonzi balingaki se kosepelisa ye ; alingi kokutana na profeta wa solo, ata akoloba makambo makosepelisa ye te.
22,15.	Mikea azongeli makambo ma­nso baprofeta balobaki mpo ya koseke bango.
22,17.	Mikea asakoli awa ’te basoda bakolonga te, mpe mokonzi akokufa.
22,30.	Eyoki ye maloba ma profeta, Akab akanisi ’te akoki kokima liwa soko alati bilamba bya soda wa mpamba.
22,34.	Moto oyo azalaki kobetele ata nani makula, nzokande azokisi moko­nzi ! Maye Nzambe akani kosala, moto moko akokima mango te.
22,49.	Se mpamba Yozafat ameki ko-sala lokola Salomo mpo akoma na nkita mingi (1 Bak 9,28 ; 10,22).
